# ZKAuth #
Front end part of library for the zero knowledge password proof that is proven hard against dictionary attacks.

git clone git@bitbucket.org:zk_capstone/frontend.git --recursive

### Tests ###
Open spec/SpecRunner.html in browser

### Back end ###
https://bitbucket.org/zk_capstone/backend