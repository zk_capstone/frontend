/*
 * Copyright (c) 2016 Gijs Van Laer, Rono Dasgupta, and Aditya Patil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
(function (exports) {
    'use strict';

    var basepath = document.currentScript.src;
    loadScript(basepath + '/../../node_modules/bn.js/lib/bn.js', function () {
        loadScript(basepath + '/../../node_modules/sjcl/sjcl.js', function () {
            loadScript(basepath + '/../../node_modules/scrypt/scrypt.js', afterLoadingLibraries);
        });
    });

    var config;
    var prime = 'F56C2A7D366E3EBDEAA1891FD2A0D099436438A673FED4D75F594959' +
        'CFFEBCA7BE0FC72E4FE67D91D801CBA0693AC4ED9E411B41D19E2FD1' +
        '699C4390AD27D94C69C0B143F1DC88932CFE2310C886412047BD9B1C' +
        '7A67F8A25909132627F51A0C866877E672E555342BDF9355347DBD43' +
        'B47156B2C20BAD9D2B071BC2FDCF9757F75C168C5D9FC43131BE162A' +
        '0756D1BDEC2CA0EB0E3B018A8B38D3EF2487782AEB9FBF99D8B30499' +
        'C55E4F61E5C7DCEE2A2BB55BD7F75FCDF00E48F2E8356BDB59D86114' +
        '028F67B8E07B127744778AFF1CF1399A4D679D92FDE7D941C5C85C5D' +
        '7BFF91BA69F9489D531D1EBFA727CFDA651390F8021719FA9F7216CE' +
        'B177BD75';
    var qPrime = 'C24ED361870B61E0D367F008F99F8A1F75525889C89DB1B673C45AF5867CB467';
    var generator = '8DC6CC814CAE4A1C05A3E186A6FE27EABA8CDB133FDCE14A963A92E8' +
        '09790CBA096EAA26140550C129FA2B98C16E84236AA33BF919CD6F58' +
        '7E048C52666576DB6E925C6CBE9B9EC5C16020F9A44C9F1C8F7A8E61' +
        '1C1F6EC2513EA6AA0B8D0F72FED73CA37DF240DB57BBB27431D61869' +
        '7B9E771B0B301D5DF05955425061A30DC6D33BB6D2A32BD0A75A0A71' +
        'D2184F506372ABF84A56AEEEA8EB693BF29A640345FA1298A16E8542' +
        '1B2208D00068A5A42915F82CF0B858C8FA39D43D704B6927E0B2F916' +
        '304E86FB6A1B487F07D8139E428BB096C6D67A76EC0B8D4EF274B8A2' +
        'CF556D279AD267CCEF5AF477AFED029F485B5597739F5D0240F67C2D' +
        '948A6279';

    function init() {
        config = {
            scrypt: {
                cpuCost: 16,
                memCost: 1,
                parallelCost: 1,
                dkLen: 64,
                saltLength: 16,
                splitChar: '||'
            },
            storageKey: 'ZKAuth.Hash',
            g: new BN(generator, 16),
            p: BN.red(new BN(prime, 16)),
            q: BN.red(new BN(qPrime, 16))
        };

        var api = {
            //register a new user (string username, string password, boolean trustedDevice)
            register: function (username, password, trustedDevice, callback) {
                var salt = computeRandomSalt();
                // For performance testing purposes:
                // ZKAuth._test.computeHash(username, password, salt, trustedDevice, function (hashedPassword) {
                computeHash(username, password, salt, trustedDevice, function (hashedPassword) {
                    var statement = createStatement(password, hashedPassword);
                    callback({
                        username: username,
                        salt: salt,
                        statement: statement.toString()
                    });
                });
            },
            //login user (string username, string password, boolean trustedDevice)
            login: function (username, password, salt, trustedDevice, callback) {
                computeOrGetHash(username, password, salt, trustedDevice, function (hashedPassword) {
                    var statement = createStatement(password, hashedPassword);
                    createProof(username, statement, password, hashedPassword, function (proof) {
                        callback({
                            username: username,
                            proof: {
                                timestamp: proof.timestamp,
                                proof: proof.proof.toString(),
                                commitment: proof.commitment.toString()
                            }
                        });
                    });
                });
            },
            cleanCache: function (username) {
                localStorage.removeItem(config.storageKey + '.' + username);
            }
        };
        if (DEBUG) {
            api._test = {
                computeRandomSalt: computeRandomSalt,
                computeOrGetHash: computeOrGetHash,
                computeHash: computeHash,
                createStatement: createStatement,
                createProof: createProof,
                cacheHash: cacheHash,
                stringToNumber: stringToNumber,
                bytesToHex: bytesToHex,
                config: config
            }
        }
        return api;
    }

    function computeRandomSalt() {
        return Crypto.util.bytesToHex(Crypto.util.randomBytes(config.scrypt.saltLength));
    }

    function computeOrGetHash(username, password, salt, trustedDevice, callback) {
        if (trustedDevice) {
            var hashedPassword = localStorage.getItem(config.storageKey + '.' + username);
            if (hashedPassword !== null) {
                var splitChar = hashedPassword.indexOf(config.scrypt.splitChar);
                if (hashedPassword.substring(0, splitChar) === salt) {
                    return callback(Crypto.charenc.Binary.stringToBytes(hashedPassword.substring(splitChar + config.scrypt.splitChar.length)));
                }
            }
        } else {
            localStorage.removeItem(config.storageKey + '.' + username);
        }
        return computeHash(username, password, salt, trustedDevice, callback);
    }

    function computeHash(username, password, salt, trustedDevice, callback) {
        Crypto_scrypt(username + password, salt, config.scrypt.cpuCost, config.scrypt.memCost,
            config.scrypt.parallelCost, config.scrypt.dkLen, function (result) {
                if (trustedDevice) {
                    cacheHash(username, result, salt);
                } else {
                    localStorage.removeItem(config.storageKey + '.' + username);
                }
                return callback(result);
            });
    }

    function cacheHash(username, result, salt) {
        localStorage.setItem(config.storageKey + '.' + username, salt + config.scrypt.splitChar + Crypto.charenc.Binary.bytesToString(result));
    }

    function createStatement(password, hashedPassword) {
        var passwordNum = stringToNumber(password);
        var hashedPasswordNum = new BN(hashedPassword);
        var power1 = config.g.toRed(config.p).redPow(passwordNum);
        var power2 = config.g.toRed(config.p).redPow(hashedPasswordNum);
        return power1.redMul(power2);
    }

    function stringToNumber(string) {
        return new BN(Crypto.charenc.Binary.stringToBytes(string));
    }

    function createProof(username, statement, password, hashedPassword, callback) {
        var passwordNum = stringToNumber(password);
        var hashedPasswordNum = new BN(hashedPassword);
        createRandomString(16, function (randomness) {
            var exponent = new BN(randomness).umod(config.q.m);
            var commitment = config.g.toRed(config.p).redPow(exponent);
            var timestamp = new Date().getTime();
            var toHash = '' + config.g + commitment + statement + username + timestamp;
            var challenge = new BN(bytesToHex(sjcl.hash.sha256.hash(toHash), 64), 16).umod(config.q.m);
            var proof = {
                timestamp: timestamp,
                commitment: commitment,
                proof: exponent.sub((passwordNum.add(hashedPasswordNum).umod(config.q.m)).mul(challenge)).umod(config.q.m)
            };
            callback(proof);
        });

    }

    function createRandomString(length, callback) {
        var randomBase64String = '',
            checkReadiness;

        checkReadiness = setInterval(function () {
            if (sjcl.random.isReady(10)) {
                while (randomBase64String.length < length) {
                    var randomInt = sjcl.random.randomWords(1, 10)[0];
                    randomBase64String += btoa(randomInt);
                }
                randomBase64String = randomBase64String.substr(0, length);
                callback(randomBase64String);
                clearInterval(checkReadiness);
            }
        }, 1);
    }

    function loadScript(url, callback) {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;

        script.onreadystatechange = callback;
        script.onload = callback;

        head.appendChild(script);
    }

    function afterLoadingLibraries() {
        if (typeof(exports.ZKAuth) === 'undefined') {
            exports.ZKAuth = init();
        } else {
            console.log('ZKAuth already defined.');
        }
    }

    function bytesToHex(byteArray) {
        for (var a = [], c = 0; c < byteArray.length; c++) {
            a.push(leadWithZeros((byteArray[c] >>> 4).toString(16), 7));
            a.push((byteArray[c] & 15).toString(16));
        }
        return a.join("");
    }

    function leadWithZeros(string, size) {
        while (string.length < size) {
            string = '0' + string;
        }
        return string;
    }
})(typeof exports != 'undefined' ? exports : window);