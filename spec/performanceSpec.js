jasmine.DEFAULT_TIMEOUT_INTERVAL = 2000;
var MAX_SAFE_TIMEOUT = Math.pow(2, 31) - 1;
const DEBUG = true;


describe('Performance tests', function () {
    beforeEach(function () {
        ZKAuth._test.config.scrypt.cpuCost = 1;
        ZKAuth._test.config.scrypt.memCost = 1;
    });

    xdescribe('scrypt', function () {
        it('should recompute hash when not in cache', function (done) {
            var cpuCost = [];
            var memCost = [];

            for(var i=2; i<6; i++) {
                memCost.push(Math.pow(2, i));
            }
            for(var j=10; j<18; j++) {
                cpuCost.push(Math.pow(2, j));
            }

            function test_scrypt(cpu, mem, callback) {
                ZKAuth._test.config.scrypt.cpuCost = cpu;
                ZKAuth._test.config.scrypt.memCost = mem;
                var startTime = new Date();
                ZKAuth._test.computeHash('user', 'password', 'salt', true, function (result) {
                    var endTime = new Date() - startTime;
                    expect(Crypto.charenc.Binary.bytesToString(result)).toEqual(Crypto.charenc.Binary.bytesToString(result));
                    console.log('cpuCost:' + ZKAuth._test.config.scrypt.cpuCost);
                    console.log('Block size parameter:' + ZKAuth._test.config.scrypt.memCost);
                    console.log(endTime);
                    callback();
                });
            }

            async.eachSeries(cpuCost, function (cpuValue, callback) {
                async.eachSeries(memCost, function (memValue, callback) {
                    test_scrypt(cpuValue, memValue, callback)
                }, callback);
            }, done);

        }, MAX_SAFE_TIMEOUT);
    });

    describe('createProof', function () {
        it('should output a number', function (done) {
            var statement = new BN('44916223149355227506602670598291058984149434344149339588255758360375860574469');
            var hashedPassword = Crypto.charenc.Binary.stringToBytes('\xb2\x10\x24\x85\xc0\x3c\xfb\x90\xa2\x06\xe1\x0b\x63\x37\xb3\x53\xd7\xb5\x6e\x1f\x8c\x63\x01\x6e\x9a\x7a\x8d\x61\x6f\xe2\xd2\x19\xa1\x88\xd9\x3d\x6b\xdc\xda\x00\x86\x9c\xbe\xfe\x9f\xe7\xe5\x6a\x51\xb7\xac\x28\x47\xeb\x6b\x3a\xe9\x03\x18\x15\xb3\x86\x18\x32');
            var startTime = new Date();
            ZKAuth._test.createProof('user',
                statement,
                'password', hashedPassword,
                function (proof) {
                    var endTime = new Date() - startTime;
                    expect(proof).toBeDefined();
                    console.log('create proof:' + endTime);
                    done();
                });
        }, MAX_SAFE_TIMEOUT);
    });

    describe('Login', function () {
        it('should return a correct object', function (done) {
            // First compute hash to store the precomputed value
            ZKAuth._test.computeHash('user', 'password', 'salt', true, function () {
                var startTime = new Date();
                ZKAuth.login('username', 'password', 'salt', true, function (loginObject) {
                    var endTime = new Date() - startTime;
                    expect(loginObject).toBeDefined();
                    console.log('login:' + endTime);
                    done();
                });
            });
        }, MAX_SAFE_TIMEOUT);
    });

    xdescribe('Register', function () {
        it('should return a correct object', function (done) {
            var hashedPassword = Crypto.charenc.Binary.stringToBytes('\xb2\x10\x24\x85\xc0\x3c\xfb\x90\xa2\x06\xe1\x0b\x63\x37\xb3\x53\xd7\xb5\x6e\x1f\x8c\x63\x01\x6e\x9a\x7a\x8d\x61\x6f\xe2\xd2\x19\xa1\x88\xd9\x3d\x6b\xdc\xda\x00\x86\x9c\xbe\xfe\x9f\xe7\xe5\x6a\x51\xb7\xac\x28\x47\xeb\x6b\x3a\xe9\x03\x18\x15\xb3\x86\x18\x32');
            var spy = spyOn(ZKAuth._test, 'computeHash');
            spy.and.callFake(function (username, password, salt, trustedDevice, callback) {
                callback(hashedPassword);
            });
            var startTime = new Date();
            ZKAuth.register('username', 'password', true, function (loginObject) {
                var endTime = new Date() - startTime;
                expect(loginObject).toBeDefined();
                expect(spy).toHaveBeenCalled();
                console.log('register:' + endTime);
                done();
            });
        }, MAX_SAFE_TIMEOUT);
    });

    describe('createStatement', function () {
        it('should be a number mod p', function () {
            var startTime = new Date();
            var statement = ZKAuth._test.createStatement('password', Crypto.charenc.Binary.stringToBytes('\xb2\x10\x24\x85\xc0\x3c\xfb\x90\xa2\x06\xe1\x0b\x63\x37\xb3\x53\xd7\xb5\x6e\x1f\x8c\x63\x01\x6e\x9a\x7a\x8d\x61\x6f\xe2\xd2\x19\xa1\x88\xd9\x3d\x6b\xdc\xda\x00\x86\x9c\xbe\xfe\x9f\xe7\xe5\x6a\x51\xb7\xac\x28\x47\xeb\x6b\x3a\xe9\x03\x18\x15\xb3\x86\x18\x32'));
            var endTime = new Date() - startTime;
            expect(statement.fromRed().lt(ZKAuth._test.config.p.m)).toBeTruthy();
            console.log('create statement:' + endTime);
        }, MAX_SAFE_TIMEOUT);
    });
});