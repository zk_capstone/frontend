jasmine.DEFAULT_TIMEOUT_INTERVAL = 2000;
var MAX_SAFE_TIMEOUT = Math.pow(2, 31) - 1;
const DEBUG = true;

describe('Test library', function () {
    beforeEach(function () {
        ZKAuth._test.config.scrypt.cpuCost = 16;
        ZKAuth._test.config.scrypt.memCost = 1;
    });

    describe('Register', function () {
        it('should return a correct object', function (done) {
            ZKAuth.register('username', 'password', true, function (registrationObject) {
                expect(registrationObject).toBeDefined();
                expect(registrationObject.username).toBeDefined();
                expect(registrationObject.statement).toBeDefined();
                expect(registrationObject.salt).toBeDefined();
                done();
            });
        },MAX_SAFE_TIMEOUT);
    });

    describe('Login', function () {
        it('should return a correct object', function (done) {
            ZKAuth.login('username', 'password', 'salt', true, function (loginObject) {
                expect(loginObject).toBeDefined();
                expect(loginObject.username).toBeDefined();
                expect(loginObject.proof.proof).toBeDefined();
                expect(loginObject.proof.timestamp).toBeDefined();
                expect(loginObject.proof.commitment).toBeDefined();
                done();
            });
        }, MAX_SAFE_TIMEOUT);
    });

    describe('computeRandomSalt', function () {
        it('should output enough salt', function () {
            expect(ZKAuth._test.computeRandomSalt().length).toEqual(ZKAuth._test.config.scrypt.saltLength * 2);
        }, MAX_SAFE_TIMEOUT);
    });

    describe('computeOrGetHash', function () {
        it('should recompute hash when not in cache', function (done) {
            localStorage.removeItem(ZKAuth._test.config.storageKey + '.user');
            ZKAuth._test.computeOrGetHash('user', 'password', 'salt', true, function (result) {
                expect(Crypto.charenc.Binary.bytesToString(result)).toEqual('\xb2\x10\x24\x85\xc0\x3c\xfb\x90\xa2\x06\xe1\x0b\x63\x37\xb3\x53\xd7\xb5\x6e\x1f\x8c\x63\x01\x6e\x9a\x7a\x8d\x61\x6f\xe2\xd2\x19\xa1\x88\xd9\x3d\x6b\xdc\xda\x00\x86\x9c\xbe\xfe\x9f\xe7\xe5\x6a\x51\xb7\xac\x28\x47\xeb\x6b\x3a\xe9\x03\x18\x15\xb3\x86\x18\x32');
                expect(localStorage.getItem(ZKAuth._test.config.storageKey + '.user')).toEqual('salt' + ZKAuth._test.config.scrypt.splitChar + Crypto.charenc.Binary.bytesToString(result));
                done();
            });
        }, MAX_SAFE_TIMEOUT);

        it('should get hash from cache', function (done) {
            localStorage.setItem(ZKAuth._test.config.storageKey + '.user', 'salt' + ZKAuth._test.config.scrypt.splitChar + 'string');
            ZKAuth._test.computeOrGetHash('user', 'password', 'salt', true, function (result) {
                expect(Crypto.charenc.Binary.bytesToString(result)).toEqual('string');
                expect(localStorage.getItem(ZKAuth._test.config.storageKey + '.user')).toEqual('salt' + ZKAuth._test.config.scrypt.splitChar + Crypto.charenc.Binary.bytesToString(result));
                done();
            });
        }, MAX_SAFE_TIMEOUT);

        it('should recompute hash when salt not the same', function (done) {
            localStorage.setItem(ZKAuth._test.config.storageKey + '.user', 'differentSalt' + ZKAuth._test.config.scrypt.splitChar + 'string');
            ZKAuth._test.computeOrGetHash('user', 'password', 'salt', true, function (result) {
                expect(Crypto.charenc.Binary.bytesToString(result)).toEqual('\xb2\x10\x24\x85\xc0\x3c\xfb\x90\xa2\x06\xe1\x0b\x63\x37\xb3\x53\xd7\xb5\x6e\x1f\x8c\x63\x01\x6e\x9a\x7a\x8d\x61\x6f\xe2\xd2\x19\xa1\x88\xd9\x3d\x6b\xdc\xda\x00\x86\x9c\xbe\xfe\x9f\xe7\xe5\x6a\x51\xb7\xac\x28\x47\xeb\x6b\x3a\xe9\x03\x18\x15\xb3\x86\x18\x32');
                expect(localStorage.getItem(ZKAuth._test.config.storageKey + '.user')).toEqual('salt' + ZKAuth._test.config.scrypt.splitChar + Crypto.charenc.Binary.bytesToString(result));
                done();
            });
        }, MAX_SAFE_TIMEOUT);

        it('should recompute hash when untrusted device', function (done) {
            localStorage.setItem(ZKAuth._test.config.storageKey + '.user', 'salt' + ZKAuth._test.config.scrypt.splitChar + 'string');
            ZKAuth._test.computeOrGetHash('user', 'password', 'salt', false, function (result) {
                expect(Crypto.charenc.Binary.bytesToString(result)).toEqual('\xb2\x10\x24\x85\xc0\x3c\xfb\x90\xa2\x06\xe1\x0b\x63\x37\xb3\x53\xd7\xb5\x6e\x1f\x8c\x63\x01\x6e\x9a\x7a\x8d\x61\x6f\xe2\xd2\x19\xa1\x88\xd9\x3d\x6b\xdc\xda\x00\x86\x9c\xbe\xfe\x9f\xe7\xe5\x6a\x51\xb7\xac\x28\x47\xeb\x6b\x3a\xe9\x03\x18\x15\xb3\x86\x18\x32');
                expect(localStorage.getItem(ZKAuth._test.config.storageKey + '.user')).toBeNull();
                done();
            });
        }, MAX_SAFE_TIMEOUT);
    });

    describe('computeHash', function () {
        it('should do callback with result', function (done) {
            ZKAuth._test.computeHash('user', 'password', 'salt', false, function (result) {
                expect(Crypto.charenc.Binary.bytesToString(result)).toEqual('\xb2\x10\x24\x85\xc0\x3c\xfb\x90\xa2\x06\xe1\x0b\x63\x37\xb3\x53\xd7\xb5\x6e\x1f\x8c\x63\x01\x6e\x9a\x7a\x8d\x61\x6f\xe2\xd2\x19\xa1\x88\xd9\x3d\x6b\xdc\xda\x00\x86\x9c\xbe\xfe\x9f\xe7\xe5\x6a\x51\xb7\xac\x28\x47\xeb\x6b\x3a\xe9\x03\x18\x15\xb3\x86\x18\x32');
                expect(localStorage.getItem(ZKAuth._test.config.storageKey + '.user')).toBeNull();
                done();
            });
        }, MAX_SAFE_TIMEOUT);

        it('should store result when trustedDevice', function (done) {
            var salt = 'salt';
            ZKAuth._test.computeHash('user', 'password', salt, true, function (result) {
                expect(localStorage.getItem(ZKAuth._test.config.storageKey + '.user')).toEqual(salt + ZKAuth._test.config.scrypt.splitChar + Crypto.charenc.Binary.bytesToString(result));
                done();
            });
        }, MAX_SAFE_TIMEOUT);
    });

    describe('cacheHash', function () {
        it('should store input', function () {
            ZKAuth._test.cacheHash('user', Crypto.charenc.Binary.stringToBytes('string'), 'salt');
            expect(localStorage.getItem(ZKAuth._test.config.storageKey + '.user')).toEqual('salt' + ZKAuth._test.config.scrypt.splitChar + 'string');
        }, MAX_SAFE_TIMEOUT);
    });

    describe('createStatement', function () {
        it('should be a number mod p', function () {
            var statement = ZKAuth._test.createStatement('password', Crypto.charenc.Binary.stringToBytes('\xb2\x10\x24\x85\xc0\x3c\xfb\x90\xa2\x06\xe1\x0b\x63\x37\xb3\x53\xd7\xb5\x6e\x1f\x8c\x63\x01\x6e\x9a\x7a\x8d\x61\x6f\xe2\xd2\x19\xa1\x88\xd9\x3d\x6b\xdc\xda\x00\x86\x9c\xbe\xfe\x9f\xe7\xe5\x6a\x51\xb7\xac\x28\x47\xeb\x6b\x3a\xe9\x03\x18\x15\xb3\x86\x18\x32'));
            expect(statement.fromRed().lt(ZKAuth._test.config.p.m)).toBeTruthy();
        }, MAX_SAFE_TIMEOUT);
    });

    describe('stringToNumber', function () {
        it('should translate string to number', function () {
            var number = ZKAuth._test.stringToNumber('This is a test string');
            expect(number).toEqual(new BN('123362224183149454759386460341821213488703364230759'));
        }, MAX_SAFE_TIMEOUT);
    });

    describe('createProof', function () {
        it('should output a number', function (done) {
            ZKAuth._test.createProof('user',
                new BN('44916223149355227506602670598291058984149434344149339588255758360375860574469'),
                'password', Crypto.charenc.Binary.stringToBytes('\xb2\x10\x24\x85\xc0\x3c\xfb\x90\xa2\x06\xe1\x0b\x63\x37\xb3\x53\xd7\xb5\x6e\x1f\x8c\x63\x01\x6e\x9a\x7a\x8d\x61\x6f\xe2\xd2\x19\xa1\x88\xd9\x3d\x6b\xdc\xda\x00\x86\x9c\xbe\xfe\x9f\xe7\xe5\x6a\x51\xb7\xac\x28\x47\xeb\x6b\x3a\xe9\x03\x18\x15\xb3\x86\x18\x32'),
                function (proof) {
                    expect(proof.proof).toBeDefined();
                    expect(proof.timestamp).toBeDefined();
                    expect(proof.commitment).toBeDefined();
                    done();
                });
        }, MAX_SAFE_TIMEOUT);

        it('should verify correctly', function (done) {
            var password = 'password';
            var hashedPassword = Crypto.charenc.Binary.stringToBytes('\xb2\x10\x24\x85\xc0\x3c\xfb\x90\xa2\x06\xe1\x0b\x63\x37\xb3\x53\xd7\xb5\x6e\x1f\x8c\x63\x01\x6e\x9a\x7a\x8d\x61\x6f\xe2\xd2\x19\xa1\x88\xd9\x3d\x6b\xdc\xda\x00\x86\x9c\xbe\xfe\x9f\xe7\xe5\x6a\x51\xb7\xac\x28\x47\xeb\x6b\x3a\xe9\x03\x18\x15\xb3\x86\x18\x32');
            var passwordNum = ZKAuth._test.stringToNumber(password);
            var hashedPasswordNum = new BN(hashedPassword);
            var power1 = ZKAuth._test.config.g.toRed(ZKAuth._test.config.p).redPow(passwordNum);
            var power2 = ZKAuth._test.config.g.toRed(ZKAuth._test.config.p).redPow(hashedPasswordNum);
            var statement = power1.redMul(power2).fromRed();
            var username = 'user';
            ZKAuth._test.createProof(username,
                statement,
                password, hashedPassword,
                function (proof) {
                    var challenge = new BN(ZKAuth._test.bytesToHex(sjcl.hash.sha256.hash('' + ZKAuth._test.config.g + proof.commitment + statement + username + proof.timestamp)), 16).umod(ZKAuth._test.config.q.m);
                    expect(ZKAuth._test.config.g.toRed(ZKAuth._test.config.p)
                        .redPow(proof.proof)
                        .redMul(statement.toRed(ZKAuth._test.config.p).redPow(challenge)).toString()).toEqual(proof.commitment.toString());
                    done();
                });
        }, MAX_SAFE_TIMEOUT);
    });
});